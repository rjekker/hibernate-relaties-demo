package nl.rjekker.demo;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 * Person class met verschillende relaties
 * Let op: alle annotaties zijn op de getters!
 * 
 * Check ook altijd de annotaties op de andere kant van de relatie!
 * 
 * - BankAccount: OneToOne
 * - Address: OneToMany, vanaf de "one" kant
 * - Nationality: OneToMany, ge-mapped vanaf de "many" kant
 * - Company: OneToMany vanaf beide kanten
 * - Clubs: ManyToMany
 * @author reindert
 *
 */
@Entity
public class Person {
	private Long id;
	private String name;
	private int age;
	private Nationality nationality;
	private Company company;
	private BankAccount bankAccount;
	private List<Club> clubs;

	/* One to One */
	@OneToOne
	public BankAccount getBankAccount() {
		return bankAccount;
	}
	public void setBankAccount(BankAccount bankAccount) {
		this.bankAccount = bankAccount;
	}

	/* One-to-many vanaf one-kant
	 * een persoon heeft maar 1 adres
	 * maar op 1 adres kunnen meerdere mensen wonen */
	private List<Address> addresses;
	@OneToMany()
	@JoinColumn(name="person_id")  // zonder deze annotatie krijg je een koppeltabel erbij
	public List<Address> getAddresses() {
		return addresses;
	}
	public void setAddresses(List<Address> addresses) {
		this.addresses = addresses;
	}
	
	/* One-to-many vanaf many-kant */
	@ManyToOne
    public Nationality getNationality() {
		return nationality;
	}
    
	public void setNationality(Nationality nationality) {
		this.nationality = nationality;
	}
	
	/* One-to-many vanaf beide kanten */
	@ManyToOne
	public Company getCompany() {
		return company;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
	
	/* Many to many */
	@ManyToMany()
	public List<Club> getClubs() {
		return clubs;
	}
	public void setClubs(List<Club> clubs) {
		this.clubs = clubs;
	}
	
	/* Id */
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
}
