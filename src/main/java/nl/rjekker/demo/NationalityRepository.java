package nl.rjekker.demo;

import org.springframework.data.repository.CrudRepository;

public interface NationalityRepository extends CrudRepository<Nationality, Long>
{
	
}
