package nl.rjekker.demo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class BankAccount {
	private String accountNumber;

	private Person holder;
	private Long id;
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	@OneToOne(mappedBy="bankAccount") // mappedBy is property op Person 
	public Person getHolder() {
		return holder;
	}
	public void setHolder(Person holder) {
		this.holder = holder;
	}
	
	
	/* Id */
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
}
