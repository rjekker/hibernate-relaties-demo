package nl.rjekker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HibernateRelatiesDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(HibernateRelatiesDemoApplication.class, args);
	}
}
