package nl.rjekker.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import nl.rjekker.demo.Nationality;
import nl.rjekker.demo.NationalityRepository;

@Controller
@RequestMapping("/nationaliteit/*")
public class NationalityController {

	@Autowired
	private NationalityRepository repo;
	
	@RequestMapping("nieuw")
	public String nieuw(){
		return "nationalityform";
	}
	
	@RequestMapping(value="nieuw", method=RequestMethod.POST)
	public String nieuw(Nationality nat, RedirectAttributes attr){
		repo.save(nat);
		attr.addFlashAttribute("msg", "Nieuwe nat aangemaakt met id " + nat.getId());
		return "redirect:/";
	}
	
	
}
