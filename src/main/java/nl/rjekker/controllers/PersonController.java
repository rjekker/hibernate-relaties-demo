package nl.rjekker.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import nl.rjekker.demo.Club;
import nl.rjekker.demo.ClubRepository;
import nl.rjekker.demo.Nationality;
import nl.rjekker.demo.NationalityRepository;
import nl.rjekker.demo.Person;
import nl.rjekker.demo.PersonRepository;

@Controller
public class PersonController {
	
	@SuppressWarnings("serial")
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public final class InvalidPersonIdException extends RuntimeException {}

	@Autowired 
	private PersonRepository personRepo;
	@Autowired 
	private NationalityRepository nationalityRepo;
	@Autowired
	private ClubRepository clubRepo;
	
	/* Homepage */
	@RequestMapping("/")
	public String index(){ return "redirect:/index.jsp"; }
	
	/* Person lijst */
	@RequestMapping("/all")
	public String all(Model model){
		model.addAttribute("persons", personRepo.findAll());
		return "all"; 
	}
	
	/* GET methodes voor form: nieuw en wijzig */
	@RequestMapping("/nieuw")
	public String nieuw(Model model){
		model.addAttribute("titel", "Persoon aanmaken");
		model.addAttribute("nationalities", nationalityRepo.findAll());
		model.addAttribute("clubs", clubRepo.findAll());
		return "personform";
	}

	@RequestMapping("/wijzig/{id}")
	public String wijzig(@PathVariable Long id, Model model){
		Person p;
		p = personRepo.findOne(id);
		if(p == null){
			throw new InvalidPersonIdException();
		}
		model.addAttribute("titel", "Persoon bewerken");
		model.addAttribute("nationalities", nationalityRepo.findAll());
		model.addAttribute("clubs", clubRepo.findAll());
		model.addAttribute("person", p);
		return "personform";
	}
	
	
	/* POST methodes voor form: nieuw en wijzig */
	@RequestMapping(value="/nieuw", method=RequestMethod.POST)
	public String nieuw(Person person, Long nationality_id,
			int[] club_ids, RedirectAttributes attr){
		if(nationality_id != null){
			// er is een nationaliteit geselecteerd
			Nationality nat = nationalityRepo.findOne(nationality_id);
			if(nat != null){
				person.setNationality(nat);
			}
		}
		
		List<Club> clubs = new ArrayList<Club>();
		
		if(club_ids != null){
			for(int cid: club_ids){
				Club c = clubRepo.findOne((long)cid);
				// we vertrouwen input vanuit form niet -> checken of dit id ergens op slaat
				if(c == null){ continue; } // dit id bestaat niet --> door met volgende
				
				if(!clubs.contains(c)){
					clubs.add(c);
				}
			}
		}
		person.setClubs(clubs);
		
		personRepo.save(person);
		attr.addFlashAttribute("msg", "Persoon toegevoegd met id " + person.getId());
		return "redirect:/wijzig/" + person.getId();
	}
	
	
	@RequestMapping(value="/wijzig/{id}", method=RequestMethod.POST)
	public String nieuw(@PathVariable Long id, Person fromForm, 
			Long nationality_id, int[] club_ids, 
			RedirectAttributes attr){
		// Let op: het Person object "fromForm" is NIET hibernate-managed maar komt uit een form
		// We moeten eerst het te wijzigen object ophalen uit de database
		// En dan de gegevens overzetten
		Person p = personRepo.findOne(id);
		if(p == null){
			return null;
		}
		
		p.setName(fromForm.getName());
		p.setAge(fromForm.getAge());
		if(nationality_id != null){
			// er is een nationaliteit geselecteerd
			Nationality nat = nationalityRepo.findOne(nationality_id);
			// we vertrouwen input vanuit form niet -> checken of dit id ergens op slaat
			if(nat != null){ 
				p.setNationality(nat);
			}
		}

		List<Club> clubs = p.getClubs();
		clubs.clear();
		if(club_ids != null){
			for(int cid: club_ids){
				Club c = clubRepo.findOne((long)cid);
				// we vertrouwen input vanuit form niet -> checken of dit id ergens op slaat
				if(c != null){
					clubs.add(c);
				} 			
				
			}
		}
		
		personRepo.save(p);
		attr.addFlashAttribute("msg", "Wijzigingen opgeslagen");
		return "redirect:/all";
	}
}
