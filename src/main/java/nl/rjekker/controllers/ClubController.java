package nl.rjekker.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import nl.rjekker.demo.Club;
import nl.rjekker.demo.ClubRepository;
import nl.rjekker.demo.PersonRepository;

@Controller
@RequestMapping("/club/*")
public class ClubController {

	@Autowired
	private ClubRepository clubs;
	
	@Autowired 
	private PersonRepository persons;
	
	@RequestMapping("nieuw")
	public String nieuw(Model model){
		model.addAttribute("persons", persons.findAll());
		return "clubform";
	}
	
	@RequestMapping(value="nieuw", method=RequestMethod.POST)
	public String nieuw(Club club, RedirectAttributes attr){
		clubs.save(club);
		attr.addFlashAttribute("msg", "Nieuwe club aangemaakt met id " + club.getId());
		return "redirect:/";
	}
	
	
}
