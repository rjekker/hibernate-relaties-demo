<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Alle Personen</title>
</head>
<body>
<div style="color: blue">
${msg}
</div>

<ul>
	<c:forEach items="${persons}" var="p">
		<li>
			<a href="/wijzig/${p.id}">${p.name}</a>
		</li>
	</c:forEach>
</ul>
</body>
</html>