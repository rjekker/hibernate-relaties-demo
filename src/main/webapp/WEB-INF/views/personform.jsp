<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${titel}</title>
</head>
<body>

<div style="color: blue">
${msg}
</div>

<form method="post">
	<table>
		<tr>
			<td>
				<label>Naam: </label>
			</td>
			<td>
				<input type="text" name="name" value="${person.name}">
			</td>
		</tr>
		<tr>
			<td>
				<label>Leeftijd: </label>
			</td>
			<td>
				<input type="number" name="age" value="${person.age}">
			</td>
		</tr>
		<tr>
			<td>
				<label>Nationaliteit: </label>
			</td>
			<td>
				<select name="nationality_id">
					<c:forEach items="${nationalities}" var="nat">
						<option value="${nat.id}"
							<c:if test="${not empty person.nationality and (nat.id == person.nationality.id)}">selected</c:if> >
							${nat.name}
						</option>
					</c:forEach>
				</select>
			</td>
			<td>
				<a href="/nationaliteit/nieuw" target="_blank">Nieuwe nationaliteit</a>
			</td>
			<tr>
			<td>
				<label>Lidmaatschappen van clubs: </label>
			</td>
			<td>
				<select name="club_ids" multiple>
					<c:forEach items="${clubs}" var="c">
						<option value="${c.id}"
							<c:if test="${not empty person.clubs and person.clubs.contains(c)}">selected</c:if> >
							${c.name}
						</option>
					</c:forEach>
				</select>
			</td>
			<td>
				<a href="/club/nieuw" target="_blank">Nieuwe club</a>
			</td>
		</tr>
	 	
	</table>
	<input type="submit">
</form>
</body>
</html>