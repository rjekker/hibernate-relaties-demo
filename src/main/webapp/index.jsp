<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Relaties Demo</title>
</head>
<body>
<div style="color: blue">
${msg}
</div>

Dit project toont verschillende Hibernate mappings voor relaties. <br>
Let erop dat je meestal aan beide kanten van de relatie annotaties nodig hebt!

<p>
<a href="/nieuw">Maak een persoon</a><br>
<a href="/nationaliteit/nieuw">Maak een nieuwe nationaliteit</a><br>
<a href="/club/nieuw">Maak een club</a><br>
<a href="/all">Lijst met personen</a><br>

</body>
</html>